# Pemilih Cerdas

Pemilih Cerdas adalah sebuah *website* yang diciptakan oleh sekelompok mahasiswa UI untuk menjawab permasalahan yang dialami oleh sistem demokrasi di Indonesia. Pemilih Cerdas bertujuan meningkatkan populasi pemilih yang melek politik. Menjelang pemilihan umum, pengguna dapat melihat visi misi dan program kerja dari para calon yang mendaftar. Pengguna website juga dapat membandingkan visi misi dua calon atau lebih dengan mudah.

## Link Website

[Pemilih Cerdas](https://pemilih-cerdas.herokuapp.com/)

## Daftar Fitur

Sampai saat ini, kami sedang mengembangkan fitur-fitur sebagai berikut:
- Searching pemilu, nama calon, serta partai politik yang terlibat.
- Halaman untuk membandingkan data diri dan program kerja dua calon.
- Halaman untuk menampilkan deskripsi lengkap seorang calon.
- Sistem pendaftaran (Sign In) bagi para calon yang hendak mempromosikan program kerja mereka.
- Sistem autentikasi (Log In) untuk para calon agar mereka dapat mengedit deskripsi program kerja mereka.

## Tim Developer
Project ini dibuat dan dipelihara oleh:
1. Inigo Ramli
2. Yobelio Ekaharja Putra
3. Monalisa Merauje
4. Muhamad Nicky
5. Erania Rajisa
