from django.db import models
from datetime import date
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

INDEPENDENT_PARTY_ID = 1

class Election(models.Model):
    """
    Stores a single election, related to :model:'core.Candidate'.
    """
    title = models.CharField(max_length=200, blank=False)
    polling_time = models.DateField(blank=True, null=True)

    @property
    def is_upcoming(self):
        return (not self.polling_time) or date.today() <= self.polling_time
    

class Party(models.Model):
    """
    Stores a single political party, related to :model:'core.Candidate'.

    """
    name = models.CharField(blank=False, max_length=100)
    profile_img = models.ImageField(null=True, upload_to='party')

class Candidate(models.Model):
    party = models.ForeignKey(Party,
        default=INDEPENDENT_PARTY_ID,
        on_delete=models.SET_DEFAULT,
        related_name='candidates',
        verbose_name='party')
    election = models.ForeignKey(Election,
        on_delete=models.SET_NULL,
        null=True,
        related_name='candidates',
        verbose_name='election')
    number = models.IntegerField()

    full_name = models.CharField(max_length=100)
    motto = models.CharField(max_length=200)
    date_of_birth = models.DateField()
    
    vision = models.TextField()
    mission = models.TextField()

    profile_img = models.ImageField(null=True, upload_to='candidate')
    file_ktp = models.FileField(upload_to='ktp')  # For ID Card, Obviously

    @property
    def party_name(self):
        return self.party.name

    @property
    def party_img(self):
        return self.party.profile_img
