# Generated by Django 2.1.5 on 2019-12-12 02:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Candidate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField()),
                ('full_name', models.CharField(max_length=100)),
                ('motto', models.CharField(max_length=200)),
                ('date_of_birth', models.DateField()),
                ('vision', models.TextField()),
                ('mission', models.TextField()),
                ('profile_img', models.ImageField(null=True, upload_to='candidate')),
                ('file_ktp', models.FileField(upload_to='ktp')),
            ],
        ),
        migrations.CreateModel(
            name='Election',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('polling_time', models.DateField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Party',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('profile_img', models.ImageField(null=True, upload_to='party')),
            ],
        ),
        migrations.CreateModel(
            name='CandidateAccount',
            fields=[
                ('username', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=50)),
                ('info', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='core.Candidate')),
            ],
        ),
        migrations.AddField(
            model_name='candidate',
            name='election',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='candidates', to='core.Election', verbose_name='election'),
        ),
        migrations.AddField(
            model_name='candidate',
            name='party',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.SET_DEFAULT, related_name='candidates', to='core.Party', verbose_name='party'),
        ),
    ]
