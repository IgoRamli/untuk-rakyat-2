import json
from django.test import TestCase
from django.urls import reverse
from django.db import IntegrityError
from django.contrib.auth.models import User
from core.models import Election, Party, Candidate

from datetime import datetime, date

ELECTION_TITLE_1 = "Pemira IKM Fasilkom 2019"
ELECTION_DATE_1 = datetime(2019, 10, 31)
ELECTION_TITLE_2 = "Fasilkom's Next Kak Pewe"
ELECTION_DATE_2 = datetime(2019, 12, 21)

PARTY_NAME_1 = 'Partai Perancangan Pewe'
PARTY_NAME_2 = 'Partai Donat Indonesia'

CANDIDATE_NAME_1 = 'Si Independen'
CANDIDATE_MOTTO_1 = 'Independen lebih baik!'
CANDIDATE_DOB_1 = datetime(2000, 9, 8)
CANDIDATE_USER_1 = 'Indy'
CANDIDATE_PASS_1 = 'admin1'

class ElectionTestCase(TestCase):

    def setUp(self):
        Election.objects.create(
            title=ELECTION_TITLE_1).save()
        Election.objects.create(
            title=ELECTION_TITLE_2,
            polling_time=ELECTION_DATE_2).save()

    def test_election_create_successful(self):
        pemira = Election.objects.filter(title__contains='Pemira')
        self.assertEqual(pemira.exists(), True, "Cannot find event 1")

        fasilkom = Election.objects.filter(title__contains='Kak Pewe')
        self.assertEqual(fasilkom.exists(), True, "Cannot find event 2")

    def test_election_date(self):
        pemira = Election.objects.filter(title__contains='Pemira')
        pemira = pemira.first()
        self.assertIsNone(pemira.polling_time, 'Blank polling time should be None')

        fasilkom = Election.objects.filter(title__contains='Kak Pewe')
        fasilkom = fasilkom.first()
        self.assertEqual(fasilkom.polling_time.year, ELECTION_DATE_2.year, 'Incorrect polling time')
        self.assertEqual(fasilkom.polling_time.month, ELECTION_DATE_2.month, 'Incorrect polling time')
        self.assertEqual(fasilkom.polling_time.day, ELECTION_DATE_2.day, 'Incorrect polling time')

    def test_election_update(self):
        Election.objects.filter(id=1).update(
            title='Pemira IKM Fasilkom UI 2019',
            polling_time=ELECTION_DATE_1)
        pemira = Election.objects.filter(title__contains='Fasilkom UI').first()
        self.assertEqual(pemira.title, 'Pemira IKM Fasilkom UI 2019', 'Incorrect Title')
        self.assertEqual(pemira.polling_time.year, ELECTION_DATE_1.year, 'Incorrect new date')
        self.assertEqual(pemira.polling_time.month, ELECTION_DATE_1.month, 'Incorrect new date')
        self.assertEqual(pemira.polling_time.day, ELECTION_DATE_1.day, 'Incorrect new date')

class CandidateTestCase(TestCase):

    def setUp(self):
        Election.objects.create(title=ELECTION_TITLE_1, polling_time=ELECTION_DATE_1)

        Party.objects.create(name='Independen')  # Indepentent party MUST be the first on the list
        Party.objects.create(name=PARTY_NAME_1)
        Party.objects.create(name=PARTY_NAME_2)

        pemira = Election.objects.all().first()
        partai_1 = Party.objects.get(id=1)
        partai_2 = Party.objects.get(id=2)

        Candidate.objects.create(election=pemira,
            party=Party.objects.get(id=1),
            number=1,
            full_name=CANDIDATE_NAME_1,
            motto=CANDIDATE_MOTTO_1,
            date_of_birth=CANDIDATE_DOB_1)
        Candidate.objects.create(election=pemira,
            party=Party.objects.get(id=3),
            number=2,
            full_name='Yobelio',
            motto='Semoga langgeng',
            date_of_birth=datetime(2000, 1, 1))

    def test_create_successful(self):
        indy = Candidate.objects.filter(full_name__contains='Independen').first()

        self.assertEqual(indy.election.title,
            'Pemira IKM Fasilkom 2019',
            'First candidate incorrect candidate election')
        self.assertEqual(indy.party.id,
            1,
            'First candidate Incorrect party affiliation')
        self.assertEqual(indy.full_name,
            'Si Independen',
            'First candidate incorrect full name')

        yobel = Candidate.objects.get(id=2)

        self.assertEqual(yobel.election.title,
            'Pemira IKM Fasilkom 2019',
            'Second candidate incorrect candidate election')
        self.assertEqual(yobel.party.name,
            'Partai Donat Indonesia',
            'Second candidate incorrect party affiliation')
        self.assertEqual(yobel.full_name,
            'Yobelio',
            'Second candidate incorrect full name')

    def test_on_candidate_party_delete(self):
        pdi = Party.objects.get(name__contains='Donat')
        pdi.delete()

        yobel = Candidate.objects.get(id=2)

        self.assertEqual(yobel.party.name,
            'Independen',
            'Second candidate should be independent now')

    def test_on_candidate_election_delete(self):
        pemira = Election.objects.get(id=1)
        pemira.delete()

        yobel = Candidate.objects.get(id=2)

        self.assertIsNone(yobel.election, 'Election should be None now')

class APITestCase(TestCase):
    def setUp(self):
        Election.objects.create(title=ELECTION_TITLE_1, polling_time=ELECTION_DATE_1)

        Party.objects.create(name='Independen')  # Indepentent party MUST be the first on the list
        Party.objects.create(name=PARTY_NAME_1)
        Party.objects.create(name=PARTY_NAME_2)

        pemira = Election.objects.all().first()
        partai_1 = Party.objects.get(id=1)
        partai_2 = Party.objects.get(id=2)

        Candidate.objects.create(election=pemira,
            party=Party.objects.get(id=1),
            number=1,
            full_name=CANDIDATE_NAME_1,
            motto=CANDIDATE_MOTTO_1,
            date_of_birth=CANDIDATE_DOB_1).save()
        Candidate.objects.create(election=pemira,
            party=Party.objects.get(id=3),
            number=2,
            full_name='Yobelio',
            motto='Semoga langgeng',
            date_of_birth=datetime(2000, 1, 1)).save()

        Election.objects.create(
            title=ELECTION_TITLE_2,
            polling_time=ELECTION_DATE_2).save()
        pewe = Election.objects.filter(title=ELECTION_TITLE_2).first()
        Candidate.objects.create(election=pewe,
            party=Party.objects.get(id=3),
            number=1,
            full_name='Monalisa',
            motto='Sekip',
            date_of_birth=datetime(1999, 6, 6)).save()

        # The great, honorable, Lord Maintainer of Pemilih Cerdas:
        self.inigo = User.objects.create_superuser('inigo', 'igoramli.igo@gmail.com', 'passwordkuat')

    def test_get_election_all(self):
        result = self.client.get(reverse('get_election'))
        # This should return ALL elections
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 2)

        result = self.client.get(reverse('get_election'), {'keyword': 'Fasilkom'})
        # This should return both elections
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 2)

    def test_get_election_partial(self):
        result = self.client.get(reverse('get_election'), {'keyword': 'Pemira'})
        # This should return only the first election
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)

        result = self.client.get(reverse('get_election'), {'keyword': 'Kak Pewe'})
        # This should return only the second election
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)

        result = self.client.get(reverse('get_election'), {'keyword': 'Donat'})
        # This should return NO elections
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 0)

    def test_get_candidate_all(self):
        # Get all candidates regardless of election
        result = self.client.get(reverse('get_candidate'))
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 3)

        # Get all candidates on one election
        result = self.client.get(reverse('get_candidate'), {'election_id': 1})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 2)

        result = self.client.get(reverse('get_candidate'), {'election_id': 2})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)

    def test_get_candidate_partial(self):
        # Get all candidates with specific name regardless of election
        result = self.client.get(reverse('get_candidate'), {'keyword': "Independen"})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)
        result = self.client.get(reverse('get_candidate'), {'keyword': "independen"})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)
        result = self.client.get(reverse('get_candidate'), {'keyword': ""})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 3)
        result = self.client.get(reverse('get_candidate'), {'keyword': "L"})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 2)

        # Get all candidates with specific name on one election
        result = self.client.get(reverse('get_candidate'), {'keyword': "independen", "election_id": 1})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)
        result = self.client.get(reverse('get_candidate'), {'keyword': "", "election_id": 2})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)
        result = self.client.get(reverse('get_candidate'), {'keyword': "O", "election_id": 1})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)
        result = self.client.get(reverse('get_candidate'), {'keyword': "O", "election_id": 2})
        self.assertEqual(result.status_code, 200)
        res_json = json.loads(result.content)
        self.assertEqual(len(res_json), 1)

    def test_post_delete_election_success(self):
        self.client.login(username='inigo', password='passwordkuat')
        result = self.client.post(reverse('del_election'), {'id': 1})
        self.assertEqual(result.status_code, 200)
        self.assertEqual(Election.objects.all().count(), 1)