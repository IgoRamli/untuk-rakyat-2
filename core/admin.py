from django.contrib import admin
from .models import Election, Party, Candidate

# Register your models here.
admin.site.register(Election)
admin.site.register(Party)
admin.site.register(Candidate)
