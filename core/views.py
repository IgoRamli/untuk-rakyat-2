import json
from itertools import chain
from datetime import datetime, date

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseForbidden
from django.forms.models import model_to_dict
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.decorators import login_required

from .models import Election, Candidate

# Create your views here.
def get_election(request):
	if request.method == 'GET':
		data = request.GET
		kw = data.get('keyword', '')

		# Get elections and separate them by polling time
		election_list_1 = Election.objects.filter(
			title__icontains=kw,
			polling_time__gte=datetime.now()
			).order_by('polling_time')
		election_list_null = Election.objects.filter(
			title__icontains=kw,
			polling_time__isnull=True
			).order_by('polling_time')
		election_list_2 = Election.objects.filter(
			title__icontains=kw,
			polling_time__lt=datetime.now()
			).order_by('-polling_time')
		batches = [election_list_1, election_list_null, election_list_2]
		election = []
		for batch in batches:
			for i in batch:
				election_dict = model_to_dict(i)
				election_dict['is_upcoming'] = i.is_upcoming
				election_dict['candidate_count'] = i.candidates.count()
				election.append(election_dict)
		res = json.dumps(election, cls=DjangoJSONEncoder)
		return HttpResponse(res)

	return HttpResponse('[]')

def get_candidate(request):
	if request.method == 'GET':
		data = request.GET
		kw = data.get('keyword', '')
		election_id = data.get('election_id', -1)

		if election_id != -1:
			candidate_list = Candidate.objects.filter(
				election_id=election_id,
				full_name__icontains=kw).all()
		else:
			candidate_list = Candidate.objects.filter(
				full_name__icontains=kw).all()

		candidate = []
		for i in candidate_list:
			candidate.append({
				'full_name': i.full_name,
				'motto': i.motto,
				'date_of_birth': i.date_of_birth,
				})
		res = json.dumps(candidate, cls=DjangoJSONEncoder)
		return HttpResponse(res)
	return HttpResponse('[]')

def del_election(request):
	if request.method == 'POST':
		# try:
			print(request.POST)
			id = request.POST['id']
			election = Election.objects.filter(id=id)
			if(election.count() != 0):
				election.first().delete()
			return HttpResponse()
		# except:
		# 	return HttpResponseForbidden()