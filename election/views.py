from django.shortcuts import render
from django.http import *
from itertools import chain

from core.models import Election, Candidate

from datetime import datetime, date

# Create your views here.

def election_list(request):
	# Election ordering:
	# 1. Upcoming / TBA Election precedes Past Election
	# 2. For Upcoming Election, Earlier Election precedes Later Election
	# 3. For Past Election, Later Election precedes Earlier Election
	# 3. TBA Election precedes Past Election and succedes Upcoming Election
	election_list_1 = Election.objects.filter(polling_time__gte=datetime.now()).order_by('polling_time')
	election_list_null = Election.objects.filter(polling_time__isnull=True).order_by('polling_time')
	election_list_2 = Election.objects.filter(polling_time__lt=datetime.now()).order_by('-polling_time')

	context = {'election_list': list(chain(election_list_1, election_list_null, election_list_2))}

	if request.user.is_superuser:
		return render(request, 'election-list-admin.html', context)
	else:
		return render(request, 'election-list.html', context)
