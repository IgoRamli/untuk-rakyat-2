from django.test import TestCase, LiveServerTestCase
from django.urls import reverse
from core.models import Election, Party, Candidate
from django.contrib.auth.models import User

import time
from datetime import datetime, date, timedelta

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options

# Create your tests here.

class ElectionArchiveTest(TestCase):
    def setUp(self):
        Election.objects.create(title="Pemira IKM Fasilkom", polling_time=datetime.now() + timedelta(days=10))
        Election.objects.create(title="The Next Kak Pewe 2019", polling_time=datetime.now() + timedelta(days=5))
        Election.objects.create(title="The Next Kak Pewe 2018", polling_time=datetime.now() - timedelta(days=10))
        Election.objects.create(title="New Election without clear date")
        # The great, honorable, Lord Maintainer of Pemilih Cerdas:
        self.inigo = User.objects.create_superuser('inigo', 'igoramli.igo@gmail.com', 'passwordkuat')

    def test_election_archive_success(self):
        response = self.client.get(reverse('election_list'))

        self.assertEqual(response.status_code, 200)
        election_list = response.context.get('election_list', [])
        self.assertEqual(len(election_list), 4)

    def test_election_archive_order(self):
        response = self.client.get(reverse('election_list'))
        election_list = response.context.get('election_list', [])

        self.assertEqual(election_list[0].title, "The Next Kak Pewe 2019")
        self.assertEqual(election_list[1].title, "Pemira IKM Fasilkom")
        self.assertEqual(election_list[2].title, "New Election without clear date")
        self.assertEqual(election_list[3].title, "The Next Kak Pewe 2018")

    def test_election_archive_as_superuser(self):
        self.client.login(username='inigo', password='passwordkuat')
        response = self.client.get(reverse('election_list'))
        self.assertTrue("admin-control" in str(response.content))

    def test_election_archive_as_user(self):
        self.client.logout()
        response = self.client.get(reverse('election_list'))
        self.assertFalse("admin-control" in str(response.content))

class ElectionArchiveFunctionalTest(LiveServerTestCase):
    def setUp(self):
        # Set up geckodriver
        binary  = FirefoxBinary('/usr/lib/firefox/firefox')
        firefox_options = Options()
        firefox_options.add_argument('--headless')
        self.browser = webdriver.Firefox(firefox_options = firefox_options, firefox_binary = binary)
        # Insert mock data to database
        Election.objects.create(title="Pemira IKM Fasilkom", polling_time=datetime.now() + timedelta(days=10)).save()
        Election.objects.create(title="The Next Kak Pewe 2019", polling_time=datetime.now() + timedelta(days=5)).save()
        Election.objects.create(title="The Next Kak Pewe 2018", polling_time=datetime.now() - timedelta(days=10)).save()
        Election.objects.create(title="New Election without clear date").save()

    def tearDown(self):
        # Bye!
        self.browser.quit()

    def test_election_archive_functional(self):
        self.browser.get(self.live_server_url + '/election');

        search_box = self.browser.find_element_by_id('search_box');
        search_btn = self.browser.find_element_by_id('search_btn');

        time.sleep(3);
        entries = self.browser.find_elements_by_class_name('entry');
        # self.assertEqual(len(entries), 4);

        search_box.send_keys('Next');
        search_btn.click();
        time.sleep(3);
        entries = self.browser.find_elements_by_class_name('entry');
        # self.assertEqual(len(entries), 2);

        search_box.clear();
        search_box.send_keys('Pemira');
        search_btn.click();
        time.sleep(3);
        entries = self.browser.find_elements_by_class_name('entry');
        # self.assertEqual(len(entries), 1);

        search_box.clear();
        search_box.send_keys('Pemilu');
        search_btn.click();
        time.sleep(3);
        entries = self.browser.find_elements_by_class_name('entry');
        # self.assertEqual(len(entries), 0);

        search_box.clear();
        search_box.send_keys('f');
        search_btn.click();
        time.sleep(3);
        entries = self.browser.find_elements_by_class_name('entry');
        # self.assertEqual(len(entries), 1);