window.onload = function() {
	getElections('');
}

function getElections(query) {
	var loc = window.location;
	var URI = loc.protocol + '//' + loc.hostname + ":" + loc.port;
	URI += '/api/election?keyword=';
	URI += document.getElementById("search_box").value;
	$.get(URI, function(data, status) {
		if(status === "success") {
			var table_body = $('#election_body');
			// Clear any data inside election table
			table_body.children().remove();
			var json = JSON.parse(data);
			json.forEach(appendElection);
		}
	});
}

function getElections(query) {
	var request = new XMLHttpRequest();
	var loc = window.location;
	var URI = loc.protocol + '//' + loc.hostname + ":" + loc.port;
	URI += '/api/election?keyword=';
	URI += document.getElementById("search_box").value;

	request.onreadystatechange = function() {
		if(this.readyState == 4 && this.status == 200) {
			var table_body = $('#election_body');
			// Clear any data inside election table
			table_body.children().remove();
			var json = JSON.parse(this.responseText);
			json.forEach(appendElection);
		}
	}

	request.open("GET", URI, true);
	request.send();
}

function appendElection(data) {
	var row = $('<tr></tr>').addClass('entry');
	var redirect = data.id + '/candidate/';
	if(data.is_upcoming === true) {
		row.append(
			$('<td></td>').append($('<b></b>').append(
				$('<a></a>').attr('href', redirect).text(data.title))),
			$('<td></td>').append($('<b></b>').text(data.polling_time)),
			$('<td></td>').append($('<b></b>').text(data.candidate_count)),
		);
	} else {
		row.append(
			$('<td></td>').append(
				$('<a></a>').attr('href', redirect).text(data.title)),
			$('<td></td>').text(data.polling_time),
			$('<td></td>').text(data.candidate_count)
		);
	}
	$('#election_body').append(row);
}