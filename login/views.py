from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import PostForm
from django.contrib.auth import authenticate, login, logout

def index(request):
    form = PostForm()
    flag = False
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password = password)
        if user is not None:
            login(request, user)
            flag = True
            response = {'form':form,'user':user,'flag':flag}
            return redirect('/login')
        else:
            response = {'form':form,'user':user,'flag':flag}
            return redirect('/login')
    else:
        response = {'form':form,'flag':flag}
        return render(request,'Login.html',response)

def logoutnih(request):
    logout(request)
    form = PostForm()
    response = {'form':form}
    return redirect(request, "success.html")
