from django import forms
from django.contrib.auth.models import User

class PostForm(forms.ModelForm):
    class Meta:
        model = User
        fields = {
            'username',
            'password',
        }

        widgets = {
            'username' : forms.TextInput(
                attrs={
					'class':'form-control',
                    'type' : 'text',
                    'required': True,
                }
            ),
			
			'password' : forms.PasswordInput(
                attrs={
					'class':'form-control',
                    'type' : 'password',
                    'required': True,
                }
            ),
        }
