window.onload = function() {
	getElections();
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function getElections(query) {
	var loc = window.location;
	var URI = loc.protocol + '//' + loc.hostname + ":" + loc.port;
	URI += '/api/election?keyword=';
	URI += document.getElementById("search_box").value;
	$.get(URI, function(data, status) {
		if(status === "success") {
			var table_body = $('#election_body');
			// Clear any data inside election table
			table_body.children().remove();
			var json = JSON.parse(data);
			json.forEach(appendElection);
		}
	});
}

function delEntry(index) {
	// Generate URI
	var loc = window.location;
	var URI = loc.protocol + '//' + loc.hostname + ":" + loc.port;
	URI += '/api/election/delete';
	// Generate Data
	var data = {
		id: index
	};
	var csrftoken = getCookie('csrftoken');
	$.ajaxSetup({
	    beforeSend: function(xhr, settings) {
	        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
	            xhr.setRequestHeader("X-CSRFToken", csrftoken);
	        }
	    }
	});

	$.post(URI, data, function(){}, 'json');
	// .done(function() {
	// 	alert("Successfully deleted!");
	// 	getElections();
	// }).fail(function() {
	// 	alert("Delete failed!");
	// });
}

function appendElection(data) {
	var row = $('<tr></tr>').addClass('entry').data('id', data.id);
	var redirect = data.id + '/candidate/';
	if(data.is_upcoming === true) {
		row.append(
			$('<td></td>').append($('<b></b>').append(
				$('<a></a>').attr('href', redirect).text(data.title))),
			$('<td></td>').append($('<b></b>').text(data.polling_time)),
			$('<td></td>').append($('<b></b>').text(data.candidate_count)),
		);
	} else {
		row.append(
			$('<td></td>').append(
				$('<a></a>').attr('href', redirect).text(data.title)),
			$('<td></td>').text(data.polling_time),
			$('<td></td>').text(data.candidate_count)
		);
	}
	$('#election_body').append(row);
}

function toggleAdminMode() {
	var is_admin_mode = $('#admin-control').is(':checked');
	if(is_admin_mode === true) {
		// Append delete and edit button to all entries
		$(".entry").each(function( index ) {
			var idx = $(this).data('id');
			var del_cell = $('<td></td>')
				.append($('<button></button>')
					.click(function(){delEntry(idx)})
					.text("Delete"))
				.addClass('admin-entry');
			$(this).append(del_cell);
		})
	} else {
		// Delete all delete button
		$(".admin-entry").remove();
	}
}