$('#masuk').submit(function(event) {
    event.preventDefault();
    //Mengambil value dari input username & Password
    var username = $('#id_username').val();
    var password = $('#id_password').val();
    console.log(username)
        //Ubah alamat url berikut, sesuaikan dengan alamat script pada komputer anda
    var url_login = '/login';

    //Ubah tulisan pada button saat click login
    $('#btnLogin').attr('value', 'Silahkan tunggu ...');

    //Gunakan jquery AJAX
    $.ajax({
        url: url_login,
        //mengirimkan username dan password ke script login.php
        data: JSON.stringify({ username: username, password: password }),
        //Method pengiriman
        type: 'POST',
        //Data yang akan diambil dari script pemroses
        dataType: 'JSON',
        //Respon jika data berhasil dikirim
        success: function(pesan) {
            if (pesan == 'ok') {
                //Arahkan ke halaman admin jika script pemroses mencetak kata ok
                window.location = url_admin;
            } else {
                //Cetak peringatan untuk username & password salah
                alert(pesan);
                $('#btnLogin').attr('value', 'Coba lagi ...');
            }
        },
    });
})