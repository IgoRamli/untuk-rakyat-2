from django.test import TestCase
from django.urls import reverse
from core.models import Election, Party, Candidate

from datetime import datetime, date, timedelta

# Create your tests here.

class CandidateArchiveTest(TestCase):
    def setUp(self):
        Election.objects.create(title="Pemira IKM Fasilkom")
        Election.objects.create(title="The Next Kak Pewe")

        Party.objects.create(name="Independen")
        Party.objects.create(name="Partai Kak Pewe (PKP)")

        Candidate.objects.create(election_id=1,
            party_id=2,
            number=1,
            full_name='Eran',
            motto='Hai',
            date_of_birth=datetime(1999, 5, 15))

        Candidate.objects.create(election_id=1,
            party_id=1,
            number=2,
            full_name='Monalisa',
            motto='Yang penting jadi',
            date_of_birth=datetime(1999, 10, 10))

        Candidate.objects.create(election_id=1,
            party_id=1,
            number=1,
            full_name='Yobelio',
            motto='Semoga langgeng',
            date_of_birth=datetime(2000, 1, 1))

        Candidate.objects.create(election_id=2,
            party_id=1,
            number=1,
            full_name='Inigo Ramli',
            motto='Bagus',
            date_of_birth=datetime(2000, 9, 8))

    def test_candidate_list_success_no_param(self):
        response = self.client.get(reverse('candidate_list', kwargs={'election_id': 1}))

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 3)

    def test_candidate_list_success_order(self):
        response = self.client.get(reverse('candidate_list', kwargs={'election_id': 1}))

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(candidate_list[0].full_name, 'Yobelio')
        self.assertEqual(candidate_list[1].full_name, 'Monalisa')
        self.assertEqual(candidate_list[2].full_name, 'Eran')

    def test_candidate_list_success_name_param(self):
        uri = reverse('candidate_list', kwargs={'election_id': 1})
        response = self.client.get(uri, {'name_contains': 'Yob'})

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 1)

    def test_candidate_list_sucess_party_param(self):
        uri = reverse('candidate_list', kwargs={'election_id': 1})
        response = self.client.get(uri, {'party_contains': 'PKP'})

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 1)
        self.assertEqual(candidate_list.first().full_name, 'Eran')

    def test_candidate_list_sucess_name_and_party_param(self):
        uri = reverse('candidate_list', kwargs={'election_id': 1})
        response = self.client.get(uri, {'name_contains': 'Yob', 'party_contains': 'PKP'})

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 0)

    def test_candidate_list_fail_no_such_election(self):
        response = self.client.get(reverse('candidate_list', kwargs={'election_id': 3}))
        self.assertEqual(response.status_code, 404)

    def test_candidate_list_success_post_request(self):
        # POST is fine, but the request content will be ignored
        uri = reverse('candidate_list', kwargs={'election_id': 1})
        response = self.client.post(uri, {'party_contains': 'PKP'})

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 3)