from django.shortcuts import render
from django.http import *
from itertools import chain

from core.models import Election, Candidate

from datetime import datetime, date

# Create your views here.
def candidate_list(request, election_id):
	# Check if election with id election_id exists
	if not Election.objects.filter(id=election_id).exists():
		return HttpResponseNotFound()

	if request.method == 'GET':
		# Get filtered result
		data = request.GET
		name_param = data.get('name_contains', '')
		party_param = data.get('party_contains', '')
		candidate_list = Candidate.objects.filter(
			election_id=election_id,
			full_name__contains=name_param,
			party__name__contains=party_param).order_by('party', 'number')
	else:
		# Return all candidates
		candidate_list = Candidate.objects.filter(
			election_id=election_id)

	context = {'election': Election.objects.get(id=election_id), 'candidate_list': candidate_list}
	return render(request, 'candidate-list.html', context)